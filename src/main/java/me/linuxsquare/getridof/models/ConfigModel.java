package me.linuxsquare.getridof.models;

import me.linuxsquare.getridof.GetRidOf;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class ConfigModel {

    private GetRidOf plugin;

    private FileConfiguration conf;
    private File pluginFolder;
    private File configFile;

    public ConfigModel(GetRidOf plugin) {
        this.plugin = plugin;
        setConf(this.plugin.getConfig());
    }

    public void loadConfig() {
        pluginFolder = new File("plugins" + System.getProperty("file.separator") + plugin.getDescription().getName());
        if(!pluginFolder.exists()) {
            pluginFolder.mkdir();
        }

        configFile = new File(pluginFolder + System.getProperty("file.separator") + "config.yml");
        if(!configFile.exists()) {
            plugin.saveDefaultConfig();
        }
    }

    public void reload() {
        configFile = new File(pluginFolder + System.getProperty("file.separator") + "config.yml");
        conf = YamlConfiguration.loadConfiguration(configFile);
    }

    public void setConf(FileConfiguration conf) {
        this.conf = conf;
    }

    public FileConfiguration getConf() {
        return this.conf;
    }
}
