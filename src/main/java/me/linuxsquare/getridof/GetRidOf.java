package me.linuxsquare.getridof;

import me.linuxsquare.getridof.commands.GetRidOfCommands;
import me.linuxsquare.getridof.listeners.EnchantmentListener;
import me.linuxsquare.getridof.listeners.EntityDropListener;
import me.linuxsquare.getridof.models.ConfigModel;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/*
    GetRidOf
    Copyright (C) 2021  LinuxSquare

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to contact me:
    E-Mail: linuxsquare@noveria.org
 */

 /*
=============================================================================================================

                        DISCLAIMER
    If you purchased this plugin from anywhere other than Spigot or Bukkit,
    I am afraid that you have been ripped off.
    Unfortunately, I can't do anything about that,
    as the plugin is released under the GNU AGPLv3 and anyone is free (as in freedom)
    to take the plugin and sell it, redistribute it, etc.

    However, if the purchase page did not mention a direct link to this or the public GitLab repository,
    I would be happy if you could send me the page where you bought the plugin.

    See contact above on how you can contact me.

    Thank you in advance
    ~ LinuxSquare

=============================================================================================================
*/

public class GetRidOf extends JavaPlugin {

    public static final String PREFIX = "§9[GetRidOf] §r";

    private ConfigModel configModel;

    @Override
    public void onEnable() {
        super.onEnable();

        configModel = new ConfigModel(this);
        configModel.loadConfig();

        getCommand("getridof").setExecutor(new GetRidOfCommands(this));

        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new EnchantmentListener(this), this);
        pm.registerEvents(new EntityDropListener(this), this);

    }


    @Override
    public void onDisable() {
        super.onDisable();
    }

    public ConfigModel getConfigModel() {
        return configModel;
    }
}