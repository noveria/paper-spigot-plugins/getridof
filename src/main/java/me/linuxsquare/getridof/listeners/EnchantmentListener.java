package me.linuxsquare.getridof.listeners;

import me.linuxsquare.getridof.GetRidOf;
import me.linuxsquare.getridof.models.ConfigModel;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemMendEvent;

public class EnchantmentListener implements Listener {

    private GetRidOf getRidOf;
    private ConfigModel conf;

    public EnchantmentListener(GetRidOf plugin) {
        this.getRidOf = plugin;
        this.conf = this.getRidOf.getConfigModel();
    }

    @EventHandler
    public void onPlayerRepairWithMending(PlayerItemMendEvent e) {
        e.setCancelled(conf.getConf().getBoolean("Settings.Enchantments.disableMending"));
    }
}
