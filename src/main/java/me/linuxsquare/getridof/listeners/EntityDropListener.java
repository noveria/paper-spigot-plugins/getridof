package me.linuxsquare.getridof.listeners;

import me.linuxsquare.getridof.GetRidOf;
import me.linuxsquare.getridof.models.ConfigModel;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class EntityDropListener implements Listener {

    private GetRidOf getRidOf;
    private ConfigModel conf;

    public EntityDropListener(GetRidOf plugin) {
        this.getRidOf = plugin;
        this.conf = this.getRidOf.getConfigModel();
    }

    @EventHandler
    public void onEntityDrop(EntityDeathEvent e) {
        Entity entity = e.getEntity();
        if(conf.getConf().contains("Settings.EntityDrops." + entity.getType())) {
            List<String> disableddrops = conf.getConf().getStringList("Settings.EntityDrops." + entity.getType());
            for(String drop : disableddrops) {
                Material dropped = Material.getMaterial(drop.toUpperCase());

                if(!e.getDrops().isEmpty()) {
                    boolean removed = e.getDrops().removeIf(itemStack -> itemStack.getType() == dropped);
                    if(removed && conf.getConf().getBoolean("Settings.displayRemovedItemDropMessage")) {
                        getRidOf.getServer().getConsoleSender().sendMessage(GetRidOf.PREFIX + "§cRemoved §6" + dropped + " §cfrom §6" + entity.getType() + "'s §cLoottable");
                    }
                }
            }
        }
    }

    private Integer lootLoop(List<ItemStack> drops, Material toRemove) {
        int i = 0;
        for(ItemStack stack : drops) {
            if(stack.getType() == toRemove) {
                return i;
            }
            i++;
        }
        return 0;
    }
}
