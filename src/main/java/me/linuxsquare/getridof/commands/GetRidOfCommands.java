package me.linuxsquare.getridof.commands;

import me.linuxsquare.getridof.GetRidOf;
import me.linuxsquare.getridof.models.ConfigModel;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class GetRidOfCommands implements CommandExecutor {

    private GetRidOf getRidOf;
    private ConfigModel conf;

    public GetRidOfCommands(GetRidOf plugin) {
        this.getRidOf = plugin;
        this.conf = this.getRidOf.getConfigModel();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(command.getName().equalsIgnoreCase("getridof")) {
            if(args.length != 1) {
                sender.sendMessage("§cInvalid Argumentamount!");
                return true;
            }

            switch (args[0]) {
                case "reload":
                        conf.reload();
                        sender.sendMessage(GetRidOf.PREFIX + "§aConfig reloaded");
                    break;
                case "version":
                    sender.sendMessage(GetRidOf.PREFIX + "§a"+getRidOf.getDescription().getName()+" "+getRidOf.getDescription().getVersion()+" §6(c) §aLinuxSquare\n" +
                            "    §aThis plugin is licensed under the §lGNU AGPLv3 §afrom the Free Software Foundation.");
                    break;
            }
        }

        return false;
    }
}
